<?php

namespace Mm\Brief10;

use Slim\Factory\AppFactory;
use Slim\Views\Twig;
use Slim\Views\TwigMiddleware;

class Main
{


    public function __construct()
    {


        $app = AppFactory::create();

        $twig = Twig::create('src/template', ['cache' => false]);

        $app->add(TwigMiddleware::create($app, $twig));

        $app->addRoutingMiddleware();
        $app->addErrorMiddleware(true, true, true);

        $app->get('/', "Mm\Brief10\Controller\DefaultController:index");
        $app->get('/home', "Mm\Brief10\Controller\DefaultController:home");


        $app->run();
    }

}
