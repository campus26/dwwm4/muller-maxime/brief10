<?php

namespace Mm\Brief10\Controller;


use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Views\Twig;

class DefaultController
{
    // public function home(Request $request, Response $response, $args)
    // {
    //     $view = Twig::fromRequest($request);
    //     return $view->render($response, 'index.html.twig', [
    //         // 'name' => $args['name']
    //     ]);
    // }

    public function index(Request $request, Response $response, $args)
    {
        $view = Twig::fromRequest($request);
        return $view->render($response, 'index.html.twig', [
             'title' => 'Home'
        ]);
    }
}
